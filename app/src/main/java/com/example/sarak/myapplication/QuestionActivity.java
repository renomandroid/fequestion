package com.example.sarak.myapplication;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;


// --- 四択問題画面クラス ---
public class QuestionActivity extends AppCompatActivity implements OnClickListener {

    // ====================== 宣言 START ======================= //
    //-----定数定義-----
    //ログ出力用
    private static String TAG = "MyApp";
    //定数文字・問
    private static String TOI = "問";
    //定数文字・改行コード
    private static String LF = "\n";
    //定数文字・セミコロン
    private static String SEMICOLON = ";";

    //-----変数定義-----
    //選んだボタンのボタン種別
    int my_answer = 0;
    //答えコード
    int answerCode = 0;
    // 問題数カウンタ
    int questionCnt = 0;
    // 問題番号
    String strNo = new String();
    // 問題番号カウント
    int cntNo = 0;

    //-----変数定義(引数)-----
    // オール問題数カウンタ
    int allQuestionCnt = 0;
    // 正解数カウンタ
    int correctCnt = 0;
    //　正解明細リスト
    StringBuffer Result = new StringBuffer();

    // ----- DataBeanの宣言 -----
    // DataBean配列の初期化
    ArrayList<QuestionDataBean> questionDBList = new ArrayList<QuestionDataBean>();
    // １行分のDataBean
    QuestionDataBean questionDB = new QuestionDataBean();
    // ====================== 宣言 E N D ======================= //

    // ====================== 処理 START ======================= //

    // ▼データ取得処理
    // ＤＢからデータを取得する
    // 取得したデータをDataBeanに設定する
    public void setData() {

        // ----- ローカル変数 -----
        // 行数カウンタ
        int yCnt = 0;
        String line = "";
        InputStream is = null;
        BufferedReader br = null;
        // １行分のデータ格納用
        String[] strList = new String[9];

        try {
            try {
                //TODO:カテゴリ画面 Intent
                // assetsフォルダ内の"questionCategpry.txt"をオープンする
                is = this.getAssets().open("questionCategory.txt");
                br = new BufferedReader(new InputStreamReader(is, "UTF8"));

                // １行ずつ読み込み、カンマで分割する
                while ((line = br.readLine()) != null) {
                    StringTokenizer st = new StringTokenizer(line, ",");
                    int xCnt = 0;

                    //１行分のデータ設定
                    //分割したトークンを出力する
                    while (st.hasMoreTokens()) {
                        strList[xCnt] = st.nextToken();
                        //列数の加算
                        xCnt++;
                    }

                    // 行数の加算
                    yCnt++;

                    // DataBeanの初期化
                    questionDB = new QuestionDataBean();

                    // １行分のデータをDataBeanに設定する
                    questionDB.setYear(strList[0]);
                    questionDB.setSeasonKbn(strList[1]);
                    questionDB.setQuestionNo(strList[2]);
                    questionDB.setDummy1(strList[3]);
                    questionDB.setDummy2(strList[4]);
                    questionDB.setDummy3(strList[5]);
                    questionDB.setDummy4(strList[6]);
                    //問題画像名
                    questionDB.setQuestionGazou(strList[7]);
                    //答えコード
                    questionDB.setAnswerId(strList[8]);

                    // 取得したデータをリストに設定する
                    questionDBList.add(questionDB);

                }
                // 取得した行数を
                // 次画面に渡す引数に格納
                allQuestionCnt = yCnt;

            } finally {
                if (is != null) is.close();
                if (br != null) br.close();
            }
        } catch (Exception e) {
            // エラー発生時の処理
        }

    }

    // ▼データ設定処理
    // DataBeanのデータを１問分取得する
    public void getData() {

        //  ローカル変数
        String fileName = "";

        // １行分のDataBeanの初期化
        questionDB = new QuestionDataBean();
        // １行分のデータをリストから取得
        questionDB = questionDBList.get(questionCnt);

        // 次画面の引数：問題数をカウント
        questionCnt++;

        // 各データの取得
        // 問題画像
        fileName = questionDB.getQuestionGazou();
        //答えコード
        answerCode = Integer.parseInt(questionDB.getAnswerId());

        // 動確用
        System.out.println("-------------------------------------------------");
        System.out.println("fileName："+ fileName);
        System.out.println("answerCode："+ answerCode);
        System.out.println("-------------------------------------------------");

        //問題画像の設定
        String folderName = "question_png/";
        ImageView imageView_question = (ImageView) findViewById(R.id.imageView_question);

        try {
            //問題画像をassets/question_pngフォルダから読み込む
            InputStream istream = this.getAssets().open(folderName + fileName);
            Bitmap bMap = BitmapFactory.decodeStream(istream);
            Matrix matrix = new Matrix();
            // 拡大比率
            double rsz_ratio_w = 4.0;
            double rsz_ratio_h = 4.0;
            // 比率をMatrixに設定
            matrix.postScale( (float)rsz_ratio_w, (float)rsz_ratio_h );
            // リサイズ画像
            Bitmap bmpRsz = Bitmap.createBitmap(bMap, 0, 0, bMap.getWidth(),bMap.getHeight(), matrix,true);

            //読み込んだ画像を設定
            imageView_question.setImageBitmap(bmpRsz);

        } catch (IOException e) {
            e.printStackTrace();
        }
        //問題Noの設定
        cntNo++;
        strNo = String.valueOf(cntNo);

        // 問題番号の設定
        TextView textView_question_no = (TextView) findViewById(R.id.textView_question_no);
        textView_question_no.setText(TOI + strNo);

//        //選択肢の設定
//        TextView textView_choices = (TextView) findViewById(R.id.textView_choices);
//        textView_choices.setText("テスト");

    }

    //画面表示処理
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // DataBeanデータ取得
        setData();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        //DataBeanデータ設定
        getData();

        //TODO:変更 ImageButton
        //ボタン１（ア）をリスナーに設定
        ImageButton button_A = findViewById(R.id.button_A);
        button_A.setOnClickListener(this);

        //ボタン２（イ）をリスナーに設定
        ImageButton button_I = findViewById(R.id.button_I);
        button_I.setOnClickListener(this);

        //ボタン３（ウ）をリスナーに設定
        ImageButton button_U = findViewById(R.id.button_U);
        button_U.setOnClickListener(this);

        //ボタン４（エ）をリスナーに設定
        ImageButton button_E = findViewById(R.id.button_E);
        button_E.setOnClickListener(this);

        //ボタン５（解説）をリスナーに設定
        //ボタンロック・非表示設定
        Button button_explanation = findViewById(R.id.button_explanation);
        button_explanation.setOnClickListener(this);
        button_explanation.setEnabled(false);
        button_explanation.setVisibility(View.INVISIBLE);

        //ボタン６（次の問題へ）をリスナーロック・非表示設定
        Button button_next = findViewById(R.id.button_next);
        button_next.setOnClickListener(this);
        button_next.setEnabled(false);
        button_next.setVisibility(View.INVISIBLE);

        //解説テキスト
        TextView textView_explanation = (TextView) findViewById(R.id.textView_explanation);
        textView_explanation.setOnClickListener(this);
        textView_explanation.setEnabled(false);
        textView_explanation.setVisibility(View.INVISIBLE);

        //イメージ１（まる）を非表示設定
        ImageView imageView_circle = (ImageView) findViewById(R.id.myImageView_circle);
        imageView_circle.setVisibility(View.INVISIBLE);

        //イメージ２（ばつ）を非表示設定
        ImageView imageView_cross = (ImageView) findViewById(R.id.myImageView_cross);
        imageView_cross.setVisibility(View.INVISIBLE);

        //正解ボタン非表示
        // ボタン　ア
        ImageView imageView_right_btn_a = (ImageView) findViewById(R.id.circle_btn_A);
        imageView_right_btn_a.setVisibility(View.INVISIBLE);
        // ボタン　イ
        ImageView imageView_right_btn_i = (ImageView) findViewById(R.id.circle_btn_I);
        imageView_right_btn_i.setVisibility(View.INVISIBLE);
        // ボタン　ウ
        ImageView imageView_right_btn_u = (ImageView) findViewById(R.id.circle_btn_U);
        imageView_right_btn_u.setVisibility(View.INVISIBLE);
        // ボタン　エ
        ImageView imageView_right_btn_e = (ImageView) findViewById(R.id.circle_btn_E);
        imageView_right_btn_e.setVisibility(View.INVISIBLE);

        //不正解ボタン非表示
        // ボタン　ア
        ImageView imageView_miss_btn_a = (ImageView) findViewById(R.id.cross_btn_A);
        imageView_miss_btn_a.setVisibility(View.INVISIBLE);
        // ボタン　イ
        ImageView imageView_miss_btn_i = (ImageView) findViewById(R.id.cross_btn_I);
        imageView_miss_btn_i.setVisibility(View.INVISIBLE);
        // ボタン　ウ
        ImageView imageView_miss_btn_u = (ImageView) findViewById(R.id.cross_btn_U);
        imageView_miss_btn_u.setVisibility(View.INVISIBLE);
        // ボタン　エ
        ImageView imageView_miss_btn_e = (ImageView) findViewById(R.id.cross_btn_E);
        imageView_miss_btn_e.setVisibility(View.INVISIBLE);

    }

    //ボタン処理
    public void onClick(View view) {

        //押下されたボタンＩＤで処理を振り分ける
        if (view != null) {
            switch (view.getId()) {

                //ボタン１（ア）処理
                case R.id.button_A:
                    my_answer = 1;
                    answerCheck(view);
                    break;

                //ボタン２（イ）処理
                case R.id.button_I:
                    my_answer = 2;
                    answerCheck(view);
                    break;

                //ボタン３（ウ）処理
                case R.id.button_U:
                    my_answer = 3;
                    answerCheck(view);
                    break;

                //ボタン４（エ）処理
                case R.id.button_E:
                    my_answer = 4;
                    answerCheck(view);
                    break;

                //解説ボタン処理
                case R.id.button_explanation:
                    //解説テキストの表示
                    setDisplayText();
                    break;

                //解説テキスト
                case R.id.textView_explanation:
                    //テキスト非表示
                    TextView textView_explanation = (TextView) findViewById(R.id.textView_explanation);
                    textView_explanation.setEnabled(false);
                    textView_explanation.setVisibility(View.INVISIBLE);
                    break;

                //次の問題へボタン処理
                case R.id.button_next:
                    //次の問題の表示
                    setDisplay();
                    break;

                default:
                    break;

            }
        }
    }

    //正誤判定処理
    public void answerCheck(View view) {

        //ボタン１（ア）
        ImageButton button_A = (ImageButton) findViewById(R.id.button_A);
        ImageView imageView_right_btn_a = (ImageView) findViewById(R.id.circle_btn_A);
        ImageView imageView_miss_btn_a = (ImageView) findViewById(R.id.cross_btn_A);
        //ボタン２（イ）
        ImageButton button_I = (ImageButton) findViewById(R.id.button_I);
        ImageView imageView_right_btn_i = (ImageView) findViewById(R.id.circle_btn_I);
        ImageView imageView_miss_btn_i = (ImageView) findViewById(R.id.cross_btn_I);
        //ボタン３（ウ）
        ImageButton button_U = (ImageButton) findViewById(R.id.button_U);
        ImageView imageView_right_btn_u = (ImageView) findViewById(R.id.circle_btn_U);
        ImageView imageView_miss_btn_u = (ImageView) findViewById(R.id.cross_btn_U);
        //ボタン４（エ）
        ImageButton button_E = (ImageButton) findViewById(R.id.button_E);
        ImageView imageView_right_btn_e = (ImageView) findViewById(R.id.circle_btn_E);
        ImageView imageView_miss_btn_e = (ImageView) findViewById(R.id.cross_btn_E);

        ImageView imageView_circle = (ImageView) findViewById(R.id.myImageView_circle);

        ImageView imageView_cross = (ImageView) findViewById(R.id.myImageView_cross);

        //選んだ解答が初期値（0）でない場合
        if (my_answer != 0) {

            //解答が一致する場合
            if (answerCode == my_answer) {
                //押下されたボタンＩＤで処理を振り分ける
                if (view != null) {
                    switch (view.getId()) {

                        //ボタン１（ア）処理
                        case R.id.button_A:
                            //押下したボタンを赤にする
                            button_A.setBackgroundColor(Color.parseColor("#FF7E82"));
                            imageView_right_btn_a.setVisibility(View.VISIBLE);
                            imageView_circle.setVisibility(View.VISIBLE);
                            break;

                        //ボタン２（イ）処理
                        case R.id.button_I:
                            //押下したボタンを赤にする
                            button_I.setBackgroundColor(Color.parseColor("#FF7E82"));
                            imageView_right_btn_i.setVisibility(View.VISIBLE);
                            imageView_circle.setVisibility(View.VISIBLE);
                            break;

                        //ボタン３（ウ）処理
                        case R.id.button_U:
                            //押下したボタンを赤にする
                            button_U.setBackgroundColor(Color.parseColor("#FF7E82"));
                            imageView_right_btn_u.setVisibility(View.VISIBLE);
                            imageView_circle.setVisibility(View.VISIBLE);
                            break;

                        //ボタン４（エ）処理
                        case R.id.button_E:
                            //押下したボタンを赤にする
                            button_E.setBackgroundColor(Color.parseColor("#FF7E82"));
                            imageView_right_btn_e.setVisibility(View.VISIBLE);
                            imageView_circle.setVisibility(View.VISIBLE);
                            break;

                        default:
                            break;

                    }

                    // 次画面の引数：正解数をカウント
                    correctCnt++;
                    // 次画面の引数：正解明細を追加する
                    // "1"は正解
                    Result.append("1");

                    // 問題数を判定
                    // 問題数カウンタが、オール問題数カウンタを超えた場合
                    if ( questionCnt >= allQuestionCnt) {

//                        // 動確用
//                        Toast ts = Toast.makeText(this,"正解ロジック", Toast.LENGTH_LONG);
//                        ts.show();

                        // ボタン５（解説）の表示、ボタン６（次の問題へ）の表示を変更
                        afterSetting4();
                        //解答ボタンのロック
                        afterSetting2();

                    // 超えていない場合、ボタンの設定
                    } else {
                        //ボタン５（解説）、ボタン６（次の問題へ）の表示
                        afterSetting1();
                        //解答ボタンのロック
                        afterSetting2();
                    }
                }

                //解答が一致しない場合
            } else {
                //押下されたボタンＩＤで処理を振り分ける
                if (view != null) {
                    switch (view.getId()) {

                        //ボタン１（ア）処理
                        case R.id.button_A:
                            //押下したボタンを青にする
                            button_A.setBackgroundColor(Color.parseColor("#7DB5F9"));
                            imageView_miss_btn_a.setVisibility(View.VISIBLE);
                            imageView_cross.setVisibility(View.VISIBLE);
                            //正しい解答のボタンの色設定
                            setAnswerColor();
                            break;

                        //ボタン２（イ）処理
                        case R.id.button_I:
                            //押下したボタンを青にする
                            button_I.setBackgroundColor(Color.parseColor("#7DB5F9"));
                            imageView_miss_btn_i.setVisibility(View.VISIBLE);
                            imageView_cross.setVisibility(View.VISIBLE);
                            //正しい解答のボタンの色設定
                            setAnswerColor();
                            break;

                        //ボタン３（ウ）処理
                        case R.id.button_U:
                            //押下したボタンを青にする
                            button_U.setBackgroundColor(Color.parseColor("#7DB5F9"));
                            imageView_miss_btn_u.setVisibility(View.VISIBLE);
                            imageView_cross.setVisibility(View.VISIBLE);
                            //正しい解答のボタンの色設定
                            setAnswerColor();
                            break;

                        //ボタン４（エ）処理
                        case R.id.button_E:
                            //押下したボタンを青にする
                            button_E.setBackgroundColor(Color.parseColor("#7DB5F9"));
                            imageView_miss_btn_e.setVisibility(View.VISIBLE);
                            imageView_cross.setVisibility(View.VISIBLE);
                            //正しい解答のボタンの色設定
                            setAnswerColor();
                            break;

                        default:
                            break;

                    }

                    // 次画面の引数：正解明細を追加する
                    // "0"は不正解
                    Result.append("0");

                    // 問題数を判定
                    // 問題数カウンタが、オール問題数カウンタを超えた場合
                    if ( questionCnt >= allQuestionCnt) {

//                        // 動確用
//                        Toast ts = Toast.makeText(this,"不正解ロジック", Toast.LENGTH_LONG);
//                        ts.show();

                        // ボタン５（解説）の表示、ボタン６（次の問題へ）の表示を変更
                        afterSetting4();
                        //解答ボタンのロック
                        afterSetting2();

                        // 超えていない場合、ボタンの設定
                    } else {
                        //ボタン５（解説）、ボタン６（次の問題へ）の表示
                        afterSetting1();
                        //解答ボタンのロック
                        afterSetting2();
                    }
                }
            }
        }
    }

    //正しい解答のボタンの色設定
    public void setAnswerColor() {

        //ボタン１（ア）
        ImageButton button_A = (ImageButton) findViewById(R.id.button_A);
        ImageView imageView_right_btn_a = (ImageView) findViewById(R.id.circle_btn_A);
        //ボタン２（イ）
        ImageButton button_I = (ImageButton) findViewById(R.id.button_I);
        ImageView imageView_right_btn_i = (ImageView) findViewById(R.id.circle_btn_I);
        //ボタン３（ウ）
        ImageButton button_U = (ImageButton) findViewById(R.id.button_U);
        ImageView imageView_right_btn_u = (ImageView) findViewById(R.id.circle_btn_U);
        //ボタン４（エ）
        ImageButton button_E = (ImageButton) findViewById(R.id.button_E);
        ImageView imageView_right_btn_e = (ImageView) findViewById(R.id.circle_btn_E);

        switch (answerCode) {

            //ボタン１（ア）処理
            case 1:
                //正しい解答のボタンを赤にする
                button_A.setBackgroundColor(Color.parseColor("#FF7E82"));
                imageView_right_btn_a.setVisibility(View.VISIBLE);
                break;

            //ボタン２（イ）処理
            case 2:
                //正しい解答のボタンを赤にする
                button_I.setBackgroundColor(Color.parseColor("#FF7E82"));
                imageView_right_btn_i.setVisibility(View.VISIBLE);
                break;

            //ボタン３（ウ）処理
            case 3:
                //正しい解答のボタンを赤にする
                button_U.setBackgroundColor(Color.parseColor("#FF7E82"));
                imageView_right_btn_u.setVisibility(View.VISIBLE);
                break;

            //ボタン４（エ）処理
            case 4:
                //正しい解答のボタンを赤にする
                button_E.setBackgroundColor(Color.parseColor("#FF7E82"));
                imageView_right_btn_e.setVisibility(View.VISIBLE);
                break;

            default:
                break;

        }
    }

    //ボタン５（解説）、ボタン６（次の問題）の表示
    public void afterSetting1() {

        //ボタン５（解説）をロック解除・表示設定
        Button button_explanation = (Button) findViewById(R.id.button_explanation);
        button_explanation.setEnabled(true);
        button_explanation.setVisibility(View.VISIBLE);

        //ボタン６（次の問題へ）をロック解除・表示設定
        Button button_next = (Button) findViewById(R.id.button_next);
        button_next.setEnabled(true);
        button_next.setVisibility(View.VISIBLE);

    }

    //解答ボタンのロック
    public void afterSetting2() {
        //ボタン１（ア）
        ImageButton button_A = (ImageButton) findViewById(R.id.button_A);
        button_A.setEnabled(false);
        //ボタン２（イ）
        ImageButton button_I = (ImageButton) findViewById(R.id.button_I);
        button_I.setEnabled(false);
        //ボタン３（ウ）
        ImageButton button_U = (ImageButton) findViewById(R.id.button_U);
        button_U.setEnabled(false);
        //ボタン４（エ）
        ImageButton button_E = (ImageButton) findViewById(R.id.button_E);
        button_E.setEnabled(false);
    }

    //初期表示状態にもどす
    public void afterSetting3() {
        //ボタン１（ア）
        ImageButton button_A = (ImageButton) findViewById(R.id.button_A);
        button_A.setEnabled(true);
        button_A.setBackgroundColor(Color.WHITE);
        //ボタン２（イ）
        ImageButton button_I = (ImageButton) findViewById(R.id.button_I);
        button_I.setEnabled(true);
        button_I.setBackgroundColor(Color.LTGRAY);
        //ボタン３（ウ）
        ImageButton button_U = (ImageButton) findViewById(R.id.button_U);
        button_U.setEnabled(true);
        button_U.setBackgroundColor(Color.LTGRAY);
        //ボタン４（エ）
        ImageButton button_E = (ImageButton) findViewById(R.id.button_E);
        button_E.setEnabled(true);
        button_E.setBackgroundColor(Color.LTGRAY);

        //ボタン５（解説）をロック・非表示設定
        Button button_explanation = (Button) findViewById(R.id.button_explanation);
        button_explanation.setEnabled(false);
        button_explanation.setVisibility(View.INVISIBLE);

        //ボタン６（次の問題へ）をロック・非表示設定
        Button button_next = (Button) findViewById(R.id.button_next);
        button_next.setEnabled(false);
        button_next.setVisibility(View.INVISIBLE);

        //イメージ１（まる）を非表示設定
        ImageView imageView_circle = (ImageView) findViewById(R.id.myImageView_circle);
        imageView_circle.setVisibility(View.INVISIBLE);

        //イメージ２（ばつ）を非表示設定
        ImageView imageView_cross = (ImageView) findViewById(R.id.myImageView_cross);
        imageView_cross.setVisibility(View.INVISIBLE);

        //正解ボタン非表示
        // ボタン　ア
        ImageView imageView_right_btn_a = (ImageView) findViewById(R.id.circle_btn_A);
        imageView_right_btn_a.setVisibility(View.INVISIBLE);
        // ボタン　イ
        ImageView imageView_right_btn_i = (ImageView) findViewById(R.id.circle_btn_I);
        imageView_right_btn_i.setVisibility(View.INVISIBLE);
        // ボタン　ウ
        ImageView imageView_right_btn_u = (ImageView) findViewById(R.id.circle_btn_U);
        imageView_right_btn_u.setVisibility(View.INVISIBLE);
        // ボタン　エ
        ImageView imageView_right_btn_e = (ImageView) findViewById(R.id.circle_btn_E);
        imageView_right_btn_e.setVisibility(View.INVISIBLE);

        //不正解ボタン非表示
        // ボタン　ア
        ImageView imageView_miss_btn_a = (ImageView) findViewById(R.id.cross_btn_A);
        imageView_miss_btn_a.setVisibility(View.INVISIBLE);
        // ボタン　イ
        ImageView imageView_miss_btn_i = (ImageView) findViewById(R.id.cross_btn_I);
        imageView_miss_btn_i.setVisibility(View.INVISIBLE);
        // ボタン　ウ
        ImageView imageView_miss_btn_u = (ImageView) findViewById(R.id.cross_btn_U);
        imageView_miss_btn_u.setVisibility(View.INVISIBLE);
        // ボタン　エ
        ImageView imageView_miss_btn_e = (ImageView) findViewById(R.id.cross_btn_E);
        imageView_miss_btn_e.setVisibility(View.INVISIBLE);

    }

    //ボタン５（解説）の表示、ボタン６（次の問題）の表示を変更
    public void afterSetting4() {

        //ボタン５（解説）をロック解除・表示設定
        Button button_explanation = (Button) findViewById(R.id.button_explanation);
        button_explanation.setEnabled(true);
        button_explanation.setVisibility(View.VISIBLE);

        //ボタン６（結果画面へ）をロック解除・表示設定
        Button button_next = (Button) findViewById(R.id.button_next);
        button_next.setText("結果画面へ");
        button_next.setEnabled(true);
        button_next.setVisibility(View.VISIBLE);

    }

    //解説テキストの表示
    public void setDisplayText() {

        TextView textView_explanation = (TextView) findViewById(R.id.textView_explanation);
        textView_explanation.setText("解説\n　問題の解説です。まだ固定");
        textView_explanation.setBackgroundColor(Color.CYAN);
        textView_explanation.setEnabled(true);
        textView_explanation.setVisibility(View.VISIBLE);

    }

    //次の問題の表示
    public void setDisplay() {

        // 問題数を判定
        // 問題数カウンタが、オール問題数カウンタを超えた場合
        if ( questionCnt >= allQuestionCnt) {

            // 結果画面へ移動

//          　// 動確用
//            Toast ts = Toast.makeText(this,"結果：" + Result + ", 正解数：" + correctCnt + "/" + allQuestionCnt, Toast.LENGTH_LONG);
//            ts.show();

            // インテントへのインスタンス生成
            Intent intent = new Intent(QuestionActivity.this, ResultActivity.class);
            //　インテントに値をセット
            intent.putExtra("Result", Result.toString());
            intent.putExtra("Correct", String.valueOf(correctCnt));
            intent.putExtra("Overall", String.valueOf(allQuestionCnt));
            // サブ画面の呼び出し
            startActivity(intent);

        // 超えていない場合、問題を設定する
        } else {
            // 次の問題のデータを取得
            getData();
            //初期表示状態にもどす
            afterSetting3();
        }

    }
    // ====================== 処理 E N D ======================= //
}