package com.example.sarak.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class CategoryActivity extends AppCompatActivity
        implements AdapterView.OnItemClickListener {

    /**
     * カテゴリテーブルのカラム名
     */
    // 大分類
    private final static String COL_CATEGORY_LCATE = "lcate";
    // 中分類
    private final static String COL_CATEGORY_MCATE = "mcate";
    // 表示名
    private final static String COL_CATEGORY_NAME = "name";

    /**
     * 問題集テーブル名のカラム名
     */
    // 年度情報
    private final static String COL_QUIZDATA_YEAR = "year";
    // 春秋区分
    private final static String COL_QUIZDATA_SEASON = "season";
    // 問題番号
    private final static String COL_QUIZDATA_QUIZNO = "quizno";
    // 大分類
    private final static String COL_QUIZDATA_LCATE = "lcate";
    // 中分類
    private final static String COL_QUIZDATA_MCATE = "mcate";
    // 小分類
    private final static String COL_QUIZDATA_SCATE = "scate";
    // 問題区分
    private final static String COL_QUIZDATA_QUIZKBN = "quizkbn";
    // 問題（画像）
    private final static String COL_QUIZDATA_QUIZIMG = "quizimg";
    // 答え区分
    private final static String COL_QUIZDATA_ANSKBN = "anskbn";

    // 大分類を判定する為の中分類の定数
    private final static String LARGE_CATEGORY = "0";

    /**
     * リストビューに設定するキー名
     */
    // リストビューの大分類の名称を判定する為のキー
    private final static String KEY_LCATE_NAME = "keyLcateName";
    // リストビューの中分類の名称を判定する為のキー
    private final static String KEY_MCATE_NAME = "keyMcateName";
    // リストビューの大分類を取得する為のキー
    private final static String KEY_LCATE_NUMBER = "keyLcateNumber";
    // リストビューの中分類を取得する為のキー
    private final static String KEY_MCATE_NUMBER = "keyMcateNumber";

    // カテゴリの問題を抽出して書き込んだファイル名
    private final static String QUIZDATA_CATEGORY_TXT = "quizdataCategory.txt";

    // カンマ
    private final static String COMMA = ",";

    // DBを扱うクラスのインスタンス化
    DBAdapter dbAdapter = new DBAdapter(this);

    // データを格納するためのArrayListを宣言
    ArrayList<HashMap<String, String>> data
            = new ArrayList<HashMap<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category);

        // アクションバーに戻るボタンを設定する
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // DBの読み込み
        dbAdapter.openDB();

        // カテゴリのDBから取得するカラムを選択する
        String[] columns = {COL_CATEGORY_LCATE, COL_CATEGORY_MCATE,
                COL_CATEGORY_NAME};

        // カテゴリテーブルから取得するWHERE文を作成する
        String selection = null;

        // 選択したカラムのデータを取得する
        Cursor cursor = dbAdapter.selectCategoryDB(columns, selection);

        // カーソルの位置を先頭のレコードに移動
        cursor.moveToFirst();

        // テーブルの内容を出力する
        do {

            int i = 0;

            // cursorオブジェクト内の大分類のデータをString型に変換
            String largeCategory = cursor.getString(i++);

            // cursorオブジェクト内の中分類のデータをString型に変換
            String middleCategory = cursor.getString(i++);

            // cursorオブジェクト内の表示名のデータをString型に変換
            String name = cursor.getString(i++);

            // HashMap型のインスタンスを生成
            HashMap<String, String> temp
                    = new HashMap<String, String>();

            // 大分類と中分類を判定する
            if (middleCategory.equals(LARGE_CATEGORY)) {

                // 大分類の表示名を関連付けて代入
                temp.put(KEY_LCATE_NAME, name);
            }

            // 中分類のテキストを設定する
            else {

                // 中分類の表示名を関連付けて代入
                temp.put(KEY_MCATE_NAME, name);
            }

            // 大分類の区分を関連付けて代入
            temp.put(KEY_LCATE_NUMBER, largeCategory);

            // 中分類の区分を関連付けて代入
            temp.put(KEY_MCATE_NUMBER, middleCategory);

            // 作成したtempをdataに追加
            data.add(temp);

        } while (cursor.moveToNext());

        // カーソルを閉じる
        cursor.close();

        // DBを閉じる
        dbAdapter.closeDB();

        // 大分類と中分類をアダプターに設定する
        SimpleAdapter adapter
                = new SimpleAdapter(CategoryActivity.this, data, R.layout.category,
                new String[]{KEY_LCATE_NAME, KEY_MCATE_NAME},
                new int[]{R.id.lcate, R.id.mcate}
        );

        // アダプターをリストビューにセット
        ListView varListView = (ListView) findViewById(R.id.ListView);
        varListView.setAdapter(adapter);

        // リストビューをクリックした時の処理を行う
        varListView.setOnItemClickListener(CategoryActivity.this);
    }

    // アクションバーに設定した戻るボタンのメソッドの定義
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // リストビューをクリックした時のメソッドの定義
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

        // リストビューをクリックした大分類を取得する
        String largeNumber = data.get(position).get(KEY_LCATE_NUMBER);

        // リストビューをクリックした中分類を取得する
        String middleNumber = data.get(position).get(KEY_MCATE_NUMBER);

        // 大分類の項目をクリックした場合、何もしない
        if (middleNumber.equals(LARGE_CATEGORY)) {

        }

        // 中分類の項目をクリックした場合、問題のDBからデータを取得し、
        // 問題画面にデータを渡す
        else {

            // DBの読み込み
            dbAdapter.openDB();

            // DBから取得するカラムを設定する
            String[] columns = {COL_QUIZDATA_YEAR, COL_QUIZDATA_SEASON, COL_QUIZDATA_QUIZNO, COL_QUIZDATA_LCATE,
                    COL_QUIZDATA_MCATE, COL_QUIZDATA_SCATE, COL_QUIZDATA_QUIZKBN, COL_QUIZDATA_QUIZIMG,
                    COL_QUIZDATA_ANSKBN};

            // 問題テーブルから取得するWHERE文を作成する
            String selection = COL_QUIZDATA_LCATE + " = " + largeNumber + " AND "
                    + COL_QUIZDATA_MCATE + " = " + middleNumber;

            // 問題テーブルからデータを取得する
            Cursor cursor = dbAdapter.selectQuizDataDB(columns, selection);

            // カーソルの位置を先頭のレコードに移動
            cursor.moveToFirst();

            // カラム数を取得する
            int columCount = cursor.getColumnCount();

            try {
                // 出力ストリームの生成
                FileOutputStream outputStream = openFileOutput(QUIZDATA_CATEGORY_TXT, MODE_PRIVATE);

                //出力ストリームのバッファーリング
                BufferedWriter saveText
                        = new BufferedWriter(
                        new OutputStreamWriter(outputStream));

                do {

                    // DBから抽出した問題データをカンマ区切りでバッファーに設定する
                    for (int i = 0; i < columCount; i++) {
                        saveText.write(cursor.getString(i));

                        // 最後のカラム以外はカンマを設定する
                        if (i != columCount - 1) {
                            saveText.write(COMMA);
                        }
                    }

                    // 改行をバッファーに設定する
                    saveText.newLine();

                } while (cursor.moveToNext());

                // バッファーのデータをファイルに書き込む
                saveText.flush();

                // 出力ストリームをクローズ
                outputStream.close();

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }

            // 画面遷移するクラスの設定する
            Intent intent = new Intent(getApplication(), QuestionActivity.class);

            // 画面遷移の時に渡すデータを設定する
            intent.putExtra("quizDataCategorytxt", QUIZDATA_CATEGORY_TXT);

            // 画面遷移を行う
            startActivity(intent);

            // DBを閉じる
            dbAdapter.closeDB();
        }
    }
}