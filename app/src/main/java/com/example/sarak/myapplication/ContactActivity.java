package com.example.sarak.myapplication;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ContactActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact);

        // アクションバーに戻るボタンを設定する
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // WebViewの設定を行う
        WebView myWebView = (WebView) findViewById(R.id.contact);
        myWebView.setWebViewClient(new WebViewClient());

        //. JavaScript 有効
        myWebView.getSettings().setJavaScriptEnabled(true);

        //. リンクタップ時に標準ブラウザを使わない
        myWebView.setWebViewClient(new WebViewClient());

        // お問い合わせのURLを設定する
        myWebView.loadUrl("https://www.renom-ltd.co.jp/contact.html");
    }

    // アクションバーに設定した戻るボタンのメソッドの定義
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

