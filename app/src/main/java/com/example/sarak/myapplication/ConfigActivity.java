package com.example.sarak.myapplication;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class ConfigActivity extends AppCompatActivity {

    // DBを扱うクラスのインスタンス化
    DBAdapter dbAdapter = new DBAdapter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        // ボタンを設定
        Button configButton = (Button) findViewById(R.id.reset_button);

        // アクションバーに戻るボタンを設定する
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // ボタンをクリックした時のメソッドを定義
        configButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // DBの読み込み
                dbAdapter.openDB();

                // 回答結果DBのデータを削除する
                dbAdapter.deleteResultDataDB();

                // DBを閉じる
                dbAdapter.closeDB();
            }
        });
    }

    // アクションバーに設定した戻るボタンのメソッドの定義
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
