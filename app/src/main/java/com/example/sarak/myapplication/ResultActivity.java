package com.example.sarak.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ResultActivity extends AppCompatActivity {

    private ProgressBar myProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        // アクションバーに戻るボタンを設定する
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // 前画面より値を取得
        Intent intent = getIntent();
        String ETCorrect = intent.getStringExtra("Correct");
        String ETOverall = intent.getStringExtra("Overall");
        String Result = intent.getStringExtra("Result");
        int Correct = Integer.parseInt(ETCorrect);
        ;
        int Overall = Integer.parseInt(ETOverall);
        ;

        // 結果計算
        double progress = 0;
        progress = 1.0 * Correct / Overall * 100;
        // 正解数表示
        TextView text = (TextView) findViewById(R.id.textView2);

        // 評価の表示
        if (progress == 100) {
            text.setText("よくがんばりました！");
        } else if (progress <= 100 && progress > 60) {
            text.setText("まぁまぁかな？");
        } else if (progress <= 60 && progress > 30) {
            text.setText("ダメダメですな(V)o￥o(V)");
        } else if (progress <= 30 && progress > 10) {
            text.setText("出直してきな！(-。-)y-゜゜゜");
        } else {
            text.setText("ちゃんと勉強したの？");
        }
        // 正解数の表示
        TextView text3 = (TextView) findViewById(R.id.textView3);
        text3.setText(Correct + "/" + Overall);

        // 結果の表示
        // 0:不正解 1:正解
        String re = "";
        for (int i = 0; i < Result.length(); i++) {
            int mon = i + 1;
            if ("0".equals(String.valueOf(Result.charAt(i)))) {
                re = re + "問" + mon + "：" + "×\n";
            } else {
                re = re + "問" + mon + "：" + "○\n";
            }
        }
        TextView text4 = (TextView) findViewById(R.id.textView4);
        text4.setText(re);

        // プログレスバー制御
        myProgressBar = (ProgressBar) findViewById(R.id.progress);
        int aaa = ((int) progress);
        myProgressBar.setProgress(aaa);

        View imageView = (View) findViewById(R.id.button_explanation);
        imageView.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), QuestionActivity.class);
                startActivity(intent);
            }
        });

        View imageView2 = (View) findViewById(R.id.button_next);
        imageView2.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    // アクションバーに設定した戻るボタンのメソッドの定義
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                finish();
                Intent intent = new Intent(getApplication(), MainActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
