package com.example.sarak.myapplication;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;


public class ReportActivity extends AppCompatActivity {

    // 解答数
    private final static String ANSWER_NUMBER = "解答数：";

    // 正解数
    private final static String CORRECT_NUMBER = "正解数：";

    // 正解率
    private final static String CORRECT_RATE = "正解率：";

    // 正解数をDBから取得するための値
    private final static String CORRECT_VALUE = "1";

    // パーセント
    private final static String PERCENT = "%";

    // 結果のカラム名
    private final static String COL_RESULTDATA_RESULT = "result";

    // DBを扱うクラスのインスタンス化
    DBAdapter dbAdapter = new DBAdapter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        // 成績表画面のテキストを設定する
        TextView answerEditText = (TextView) findViewById(R.id.answerText);
        TextView correctEditText = (TextView) findViewById(R.id.correctText);
        TextView rateEditText = (TextView) findViewById(R.id.rateText);

        // アクションバーに戻るボタンを設定する
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // DBの読み込みaa
        dbAdapter.openDB();

        // 回答結果のDBから取得するカラムを選択する
        String[] columns = new String[0];

        // 回答結果のDBから全てのデータを取得する条件を設定する
        String questionSelection = null;

        // 回答結果のDBから全てのデータを取得する
        Cursor questionCursor = dbAdapter.selectResultDataDB(columns, questionSelection);
        Integer answerNumber = questionCursor.getCount();

        // 正解数を取得する条件を設定する
        String correctSelection = COL_RESULTDATA_RESULT + " = " + CORRECT_VALUE;

        // 回答結果のDBから正解数のデータを取得する
        Cursor correctCursor = dbAdapter.selectResultDataDB(columns, correctSelection);
        Integer correctNumber = correctCursor.getCount();

        // 正解率の計算
        double progress = 0;
        if(answerNumber != 0 ){
            progress = 1.0 * correctNumber / answerNumber * 100;
        }else{
            progress = 0;
        }


        // カーソルを閉じる
        questionCursor.close();
        correctCursor.close();

        // DBを閉じる
        dbAdapter.closeDB();

        // 成績表から読み込んだデータをテキストビューに設定する
        answerEditText.setText(ANSWER_NUMBER + answerNumber);
        correctEditText.setText(CORRECT_NUMBER + correctNumber);
        rateEditText.setText(CORRECT_RATE + progress + PERCENT);
    }

    // アクションバーに設定した戻るボタンのメソッドの定義
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
