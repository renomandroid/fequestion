package com.example.sarak.myapplication;

import java.io.Serializable;

/**
 * Created by 151002 on 2018/01/10.
 */

//publicクラスを宣言しています。
//コンストラクタはデフォルトコンストラクタを使用します。

public class QuestionDataBean  implements Serializable {

    //プロパティidをprivate変数で宣言しています。
    //(1)データを保存する変数を宣言します。
    //年度
    private String year;
    //春秋区分
    private String seasonKbn;
    //問題番号
    private String questionNo;
    //ダミー１
    private String dummy1;
    //ダミー２
    private String dummy2;
    //ダミー３
    private String dummy3;
    //ダミー４
    private String dummy4;
    //問題（画像）
    private String questionGazou;
    //答え
    private String answerId;

    //プロパティidのsetメソッドを宣言しています。
    //(2)変数にデータを保存するメソッドを宣言します。
    //年度
    public void setYear(String year) { this.year = year; }
    //春秋区分
    public void setSeasonKbn(String seasonKbn) { this.seasonKbn = seasonKbn; }
    //問題番号
    public void setQuestionNo(String questionNo) { this.questionNo = questionNo; }
    //ダミー１
    public void setDummy1(String dummy1) { this.dummy1 = dummy1; }
    //ダミー２
    public void setDummy2(String dummy2) { this.dummy2 = dummy2; }
    //ダミー３
    public void setDummy3(String dummy3) { this.dummy3 = dummy3; }
    //ダミー４
    public void setDummy4(String dummy4) { this.dummy4 = dummy4; }
    //問題（画像）
    public void setQuestionGazou(String questionGazou) { this.questionGazou = questionGazou; }
    //答え
    public void setAnswerId(String answerId) { this.answerId = answerId; }

    //プロパティidのgetメソッドを宣言しています。
    //(3)変数のデータを返すメソッドを宣言します。
    //年度の取得
    public String getYear() { return this.year; }
    //春秋区分
    public String getSeasonKbn() { return this.seasonKbn; }
    //問題番号
    public String getQuestionNo() { return this.questionNo; }
    //ダミー１
    public String getDummy1() { return this.dummy1; }
    //ダミー２
    public String getDummy2() { return this.dummy2; }
    //ダミー３
    public String getDummy3() { return this.dummy3; }
    //ダミー４
    public String getDummy4() { return this.dummy4; }
    //問題（画像）
    public String getQuestionGazou() { return this.questionGazou; }
    //答え
    public String getAnswerId() { return this.answerId; }

}