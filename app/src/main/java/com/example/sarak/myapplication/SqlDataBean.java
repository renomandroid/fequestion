package com.example.sarak.myapplication;

import java.io.Serializable;

/**
 * Created by 151002 on 2017/08/15.
 */

//publicクラスを宣言しています。
//コンストラクタはデフォルトコンストラクタを使用します。

public class SqlDataBean  implements Serializable {

    //プロパティidをprivate変数で宣言しています。
    //(1)データを保存する変数を宣言します。
    //問題No
    private String id;
    //問題文
    private String question;
    //選択肢文
    private String choices;
    //答えコード
    private String answer;


    //プロパティidのsetメソッドを宣言しています。
    //(2)変数にデータを保存するメソッドを宣言します。
    //問題No
    public void setId(String id) { this.id = id; }
    //問題文
    public void setQuestion(String question) { this.question = question; }
    //選択肢文
    public void setChoices(String choices) { this.choices = choices; }
    //答え
    public void setAnswer(String answer) { this.answer = answer; }


    //プロパティidのgetメソッドを宣言しています。
    //(3)変数のデータを返すメソッドを宣言します。
    //問題No
    public String getId() { return this.id; }
    //問題文
    public String getQuestion() { return this.question; }
    //選択肢文
    public String getChoices() { return this.choices; }
    //答え
    public String getAnswer() { return this.answer; }

}