package com.example.sarak.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Databaseに関連するクラス
 * DBAdapter
 */
public class DBAdapter {

    // DB名
    private final static String DB_NAME = "Fedb.db";

    /**
     * テーブル名
     */
    // カテゴリのテーブル名
    private final static String DB_TABLE_CATEGORY = "Category";
    // 問題集のテーブル名
    private final static String DB_TABLE_QUIZDATA = "QuizData";
    // 回答状況のテーブル名
    private final static String DB_TABLE_ANSWER = "answertbl";
    // 回答結果のテーブル名
    private final static String DB_TABLE_RESULTDATA = "ResultData";

    // DBのバージョン
    private final static int DB_VERSION = 1;

    /**
     * カテゴリテーブルのカラム名
     */
    // id
    private final static String COL_CATEGORY_ID = "id";
    // 項目表示番号
    private final static String COL_CATEGORY_DISPLAY_NO = "displayno";
    // 大分類
    private final static String COL_CATEGORY_LCATE = "lcate";
    // 中分類
    private final static String COL_CATEGORY_MCATE = "mcate";
    // 小分類
    private final static String COL_CATEGORY_SCATE = "scate";
    // 表示名
    private final static String COL_CATEGORY_NAME = "name";
    // 春秋区分
    private final static String COL_CATEGORY_SEASON = "season";

    /**
     * 問題集テーブル名のカラム名
     */
    // id
    private final static String COL_QUIZDATA_ID = "id";
    // 年度情報
    private final static String COL_QUIZDATA_YEAR = "year";
    // 春秋区分
    private final static String COL_QUIZDATA_SEASON = "season";
    // 問題番号
    private final static String COL_QUIZDATA_NUMBER = "quizno";
    // 大分類
    private final static String COL_QUIZDATA_LCATE = "lcate";
    // 中分類
    private final static String COL_QUIZDATA_MCATE = "mcate";
    // 小分類
    private final static String COL_QUIZDATA_SCATE = "scate";
    // 問題区分
    private final static String COL_QUIZDATA_QUIZKBN = "quizkbn";
    // 問題文
    private final static String COL_QUIZDATA_QUIZTXT = "quiztxt";
    // 問題（画像）
    private final static String COL_QUIZDATA_QUIZIMG = "quizimg";
    // 答え区分
    private final static String COL_QUIZDATA_ANSKBN = "anskbn";
    // 答え（文）
    private final static String COL_QUIZDATA_ANSTXT = "anstxt";
    // 答え（画像）
    private final static String COL_QUIZDATA_ANSIMG = "ansimg";
    // 答え
    private final static String COL_QUIZDATA_ANSWER = "answer";
    // 解説区分
    private final static String COL_QUIZDATA_COMKBN = "comkbn";
    // 解説（文）
    private final static String COL_QUIZDATA_COMTXT = "comtxt";
    // 解説（画像）
    private final static String COL_QUIZDATA_COMIMG = "comimg";

    /**
     * 回答状況テーブル名のカラム名
     */
    // id
    private final static String COL_ANSWER_ID = "id";
    // 問題年度
    private final static String COL_ANSWER_YEAR = "year";
    // 春秋区分
    private final static String COL_ANSWER_SEASON = "season";
    // 本番区分
    private final static String COL_ANSWER_CATEGORY = "category";
    // 回答番号
    private final static String COL_ANSWER_NUMBER = "number";

    /**
     * 回答結果テーブル名のカラム名
     */
    // id
    private final static String COL_RESULTDATA_ID = "id";
    // 問題年度
    private final static String COL_RESULTDATA_YEAR = "year";
    // 春秋区分
    private final static String COL_RESULTDATA_SEASON = "season";
    // 問題番号
    private final static String COL_RESULTDATA_QUIZNO = "quizno";
    // 通し・カテゴリ
    private final static String COL_RESULTDATA_ANSWERTYPE = "answertype";
    // 結果
    private final static String COL_RESULTDATA_RESULT = "result";

    // SQLiteDatabase
    public SQLiteDatabase db = null;
    // DBHepler
    public DBHelper dbHelper = null;
    // Context
    public Context context;

    // コンストラクタ
    public DBAdapter(Context context) {
        this.context = context;
        dbHelper = new DBHelper(this.context);
    }

    /**
     * DBの読み書き
     * openDB()
     *
     * @return this 自身のオブジェクト
     */
    public DBAdapter openDB() {
        // DBの読み書き
        db = dbHelper.getWritableDatabase();
        return this;
    }

    /**
     * DBを閉じる
     * closeDB()
     */
    public void closeDB() {
        // DBを閉じる
        db.close();
        db = null;
    }

    /**
     * カテゴリDBのレコードへ登録
     * insertCategoryDB()
     *
     * @param columnArgs
     */
    public void insertCategoryDB(String[] columnArgs) {

        // トランザクション開始
        db.beginTransaction();

        try {

            // ContentValuesでデータを設定していく
            ContentValues values = new ContentValues();
            int i = 0;

            values.put(COL_CATEGORY_LCATE, columnArgs[i++]);
            values.put(COL_CATEGORY_MCATE, columnArgs[i++]);
            values.put(COL_CATEGORY_SCATE, columnArgs[i++]);
            values.put(COL_CATEGORY_NAME, columnArgs[i++]);

            // insertメソッド データ登録
            // 第1引数：DBのテーブル名
            // 第2引数：更新する条件式
            // 第3引数：ContentValues
            // レコードへ登録
            db.insert(DB_TABLE_CATEGORY, null, values);

            // トランザクションへコミット
            db.setTransactionSuccessful();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            // トランザクションの終了
            db.endTransaction();
        }
    }

    /**
     * 問題集DBのレコードへ登録
     * insertQuizDataDB()
     *
     * @param columnArgs
     */
    public void insertQuizDataDB(String[] columnArgs) {

        // トランザクション開始
        db.beginTransaction();

        try {

            // ContentValuesでデータを設定していく
            ContentValues values = new ContentValues();
            int i = 0;

            values.put(COL_QUIZDATA_YEAR, columnArgs[i++]);
            values.put(COL_QUIZDATA_SEASON, columnArgs[i++]);
            values.put(COL_QUIZDATA_NUMBER, columnArgs[i++]);
            values.put(COL_QUIZDATA_LCATE, columnArgs[i++]);
            values.put(COL_QUIZDATA_MCATE, columnArgs[i++]);
            values.put(COL_QUIZDATA_SCATE, columnArgs[i++]);
            values.put(COL_QUIZDATA_QUIZKBN, columnArgs[i++]);
            values.put(COL_QUIZDATA_QUIZTXT, columnArgs[i++]);
            values.put(COL_QUIZDATA_QUIZIMG, columnArgs[i++]);
            values.put(COL_QUIZDATA_ANSKBN, columnArgs[i++]);
            values.put(COL_QUIZDATA_ANSTXT, columnArgs[i++]);
            values.put(COL_QUIZDATA_ANSIMG, columnArgs[i++]);
            values.put(COL_QUIZDATA_ANSWER, columnArgs[i++]);
            values.put(COL_QUIZDATA_COMKBN, columnArgs[i++]);
            values.put(COL_QUIZDATA_COMTXT, columnArgs[i++]);
            values.put(COL_QUIZDATA_COMIMG, columnArgs[i++]);

            // insertメソッド データ登録
            // 第1引数：DBのテーブル名
            // 第2引数：更新する条件式
            // 第3引数：ContentValues
            // レコードへ登録
            db.insert(DB_TABLE_QUIZDATA, null, values);

            // トランザクションへコミット
            db.setTransactionSuccessful();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            // トランザクションの終了
            db.endTransaction();
        }
    }

    /**
     * 回答結果DBのレコードへ追加を行う
     * insertResultDataDB()
     *
     * @param columnArgs
     */
    public void insertResultDataDB(String[] columnArgs) {

        // トランザクション開始
        db.beginTransaction();

        try {

            // ContentValuesでデータを設定していく
            ContentValues values = new ContentValues();
            int i = 0;

            values.put(COL_RESULTDATA_YEAR, columnArgs[i++]);
            values.put(COL_RESULTDATA_SEASON, columnArgs[i++]);
            values.put(COL_RESULTDATA_QUIZNO, columnArgs[i++]);
            values.put(COL_RESULTDATA_ANSWERTYPE, columnArgs[i++]);
            values.put(COL_RESULTDATA_RESULT, columnArgs[i++]);

            // データを追加する
            db.insert(DB_TABLE_RESULTDATA, null, values);

            // トランザクションへコミット
            db.setTransactionSuccessful();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            // トランザクションの終了
            db.endTransaction();
        }
    }

    /**
     * カテゴリDBのデータを取得
     * selectCategoryDB()
     *
     * @param columns   String[] 取得するカラム名 nullの場合は全カラムを取得
     * @param selection
     * @return カテゴリDBのデータ
     */
    public Cursor selectCategoryDB(String[] columns, String selection) {

        // queryメソッド DBのデータを取得
        // 第1引数：DBのテーブル名
        // 第2引数：取得するカラム名
        // 第3引数：選択条件(WHERE句)
        // 第4引数：第3引数のWHERE句において?を使用した場合に使用
        // 第5引数：集計条件(GROUP BY句)
        // 第6引数：選択条件(HAVING句)
        // 第7引数：ソート条件(ODERBY句)

        // 全カラムのデータを取得する
        if (columns.length == 0) {
            return db.query(DB_TABLE_CATEGORY, null, selection, null, null, null, null);
        }

        // 指定したカラムのデータを取得する
        else {
            return db.query(DB_TABLE_CATEGORY, columns, selection, null, null, null, null);
        }
    }

    /**
     * 問題集DBのデータを取得
     * selectQuizDataDB()
     *
     * @param columns   String[] 取得するカラム名 nullの場合は全カラムを取得
     * @param selection String WHERE文
     * @return selectQuizDataDBのデータ
     */
    public Cursor selectQuizDataDB(String[] columns, String selection) {

        // queryメソッド DBのデータを取得
        // 第1引数：DBのテーブル名
        // 第2引数：取得するカラム名
        // 第3引数：選択条件(WHERE句)
        // 第4引数：第3引数のWHERE句において?を使用した場合に使用
        // 第5引数：集計条件(GROUP BY句)
        // 第6引数：選択条件(HAVING句)
        // 第7引数：ソート条件(ODERBY句)


        // 全カラムのデータを取得する
        if (columns.length == 0) {
            return db.query(DB_TABLE_QUIZDATA, null, selection, null, null, null, null);
        }

        // 指定したカラムのデータを取得する
        else {
            return db.query(DB_TABLE_QUIZDATA, columns, selection, null, null, null, null);
        }
    }

    /**
     * 回答結果DBのデータを取得
     * selectResultDataDB()
     *
     * @param columns   String[] 取得するカラム名 nullの場合は全カラムを取得
     * @param selection String WHERE文
     * @return selectResultDataDBのデータ
     */
    public Cursor selectResultDataDB(String[] columns, String selection) {

        // queryメソッド DBのデータを取得
        // 第1引数：DBのテーブル名
        // 第2引数：取得するカラム名
        // 第3引数：選択条件(WHERE句)
        // 第4引数：第3引数のWHERE句において?を使用した場合に使用
        // 第5引数：集計条件(GROUP BY句)
        // 第6引数：選択条件(HAVING句)
        // 第7引数：ソート条件(ODERBY句)

        // 全カラムのデータを取得する
        if (columns.length == 0) {
            return db.query(DB_TABLE_RESULTDATA, null, selection, null, null, null, null);
        }

        // 指定したカラムのデータを取得する
        else {
            return db.query(DB_TABLE_RESULTDATA, columns, selection, null, null, null, null);
        }
    }

    /**
     * 回答結果DBのレコードの更新を行う
     * updateResultDataDB()
     *
     * @param columnArgs
     */
    public void updateResultDataDB(String[] columnArgs, String selection) {

        // トランザクション開始
        db.beginTransaction();

        try {

            // ContentValuesでデータを設定していく
            ContentValues values = new ContentValues();
            int i = 0;

            values.put(COL_RESULTDATA_YEAR, columnArgs[i++]);
            values.put(COL_RESULTDATA_SEASON, columnArgs[i++]);
            values.put(COL_RESULTDATA_QUIZNO, columnArgs[i++]);
            values.put(COL_RESULTDATA_ANSWERTYPE, columnArgs[i++]);
            values.put(COL_RESULTDATA_RESULT, columnArgs[i++]);

            // データを追加する
            db.update(DB_TABLE_RESULTDATA, values, selection,null);

            // トランザクションへコミット
            db.setTransactionSuccessful();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            // トランザクションの終了
            db.endTransaction();
        }
    }

    /**
     * 回答結果DBのレコードを全て削除
     * deleteResultDataDB()
     */
    public void deleteResultDataDB() {

        // トランザクション開始
        db.beginTransaction();

        try {

            // データがある場合は更新、無い場合は追加する
            db.delete(DB_TABLE_RESULTDATA, null,null);

            // トランザクションへコミット
            db.setTransactionSuccessful();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            // トランザクションの終了
            db.endTransaction();
        }
    }

    /**
     * データベースの生成やアップグレードを管理するSQLiteOpenHelperを継承したクラス
     * DBHelper
     */
    private static class DBHelper extends SQLiteOpenHelper {

        // コンストラクタ
        public DBHelper(Context context) {

            //第1引数：コンテキスト
            //第2引数：DB名
            //第3引数：factory nullでよい
            //第4引数：DBのバージョン
            super(context, DB_NAME, null, DB_VERSION);
        }

        /**
         * DB生成時に呼ばれる
         * onCreate()
         *
         * @param db SQLiteDatabase
         */
        @Override
        public void onCreate(SQLiteDatabase db) {

            //カテゴリのテーブルを作成するSQL文の定義 ※スペースに気を付ける
            String createTblSubmenu = "CREATE TABLE " + DB_TABLE_CATEGORY + "("
                    + COL_CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COL_CATEGORY_DISPLAY_NO + " TEXT, "
                    + COL_CATEGORY_LCATE + " INTEGER, "
                    + COL_CATEGORY_MCATE + " INTEGER, "
                    + COL_CATEGORY_SCATE + " INTEGER, "
                    + COL_CATEGORY_NAME + " TEXT, "
                    + COL_CATEGORY_SEASON + " TEXT"
                    + ")";

            //問題集のテーブルを作成するSQL文の定義 ※スペースに気を付ける
            String createTblQuestion = "CREATE TABLE " + DB_TABLE_QUIZDATA + "("
                    + COL_QUIZDATA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COL_QUIZDATA_YEAR + " INTEGER, "
                    + COL_QUIZDATA_SEASON + " TEXT, "
                    + COL_QUIZDATA_NUMBER + " TEXT, "
                    + COL_QUIZDATA_LCATE + " INTEGER, "
                    + COL_QUIZDATA_MCATE + " INTEGER, "
                    + COL_QUIZDATA_SCATE + " INTEGER, "
                    + COL_QUIZDATA_QUIZKBN + " INTEGER, "
                    + COL_QUIZDATA_QUIZTXT + " TEXT, "
                    + COL_QUIZDATA_QUIZIMG + " TEXT, "
                    + COL_QUIZDATA_ANSKBN + " INTEGER, "
                    + COL_QUIZDATA_ANSTXT + " TEXT, "
                    + COL_QUIZDATA_ANSIMG + " TEXT, "
                    + COL_QUIZDATA_ANSWER + " INTEGER, "
                    + COL_QUIZDATA_COMKBN + " INTEGER, "
                    + COL_QUIZDATA_COMTXT + " TEXT, "
                    + COL_QUIZDATA_COMIMG + " TEXT"
                    + ")";

            //回答状況のテーブルを作成するSQL文の定義 ※スペースに気を付ける
            String createTblAnswer = "CREATE TABLE " + DB_TABLE_ANSWER + " ("
                    + COL_ANSWER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COL_ANSWER_YEAR + " INTEGER, "
                    + COL_ANSWER_SEASON + " TEXT, "
                    + COL_ANSWER_CATEGORY + " INTEGER, "
                    + COL_ANSWER_NUMBER + " TEXT"
                    + ")";

            //回答結果のテーブルを作成するSQL文の定義 ※スペースに気を付ける
            String createTblAnswerResult = "CREATE TABLE " + DB_TABLE_RESULTDATA + " ("
                    + COL_RESULTDATA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COL_RESULTDATA_YEAR + " INTEGER, "
                    + COL_RESULTDATA_SEASON + " TEXT, "
                    + COL_RESULTDATA_QUIZNO + " TEXT, "
                    + COL_RESULTDATA_ANSWERTYPE + " TEXT, "
                    + COL_RESULTDATA_RESULT + " TEXT"
                    + ")";

            //SQL文の実行
            db.execSQL(createTblSubmenu);
            db.execSQL(createTblQuestion);
            db.execSQL(createTblAnswer);
            db.execSQL(createTblAnswerResult);
        }

        /**
         * DBアップグレード(バージョンアップ)時に呼ばれる
         *
         * @param db         SQLiteDatabase
         * @param oldVersion int 古いバージョン
         * @param newVersion int 新しいバージョン
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            // DBからテーブル削除
            db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_CATEGORY);
            db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_QUIZDATA);
            db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_ANSWER);
            db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_RESULTDATA);

            // テーブル生成
            onCreate(db);
        }
    }
}