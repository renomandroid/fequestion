package com.example.sarak.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by 101009 on 2017/10/25.
 */

public class splash extends Activity {

    // カテゴリのDBに設定するファイル名
    private final static String CATEGORY_TXT = "category.txt";

    // 問題集のDBに設定するファイル名
    private final static String QUIZDATA_TXT = "quizData.txt";

    // カンマ
    private final static String COMMA = ",";

    // 設定を保存する設定データ名
    private final static String CONFIG_DATA_SAVE = "configDataSave";

    // 初回起動時用の設定を保存するデータ名
    private final static String FIRST_DATA = "firstData";

    // DBを扱うクラスのインスタンス化
    DBAdapter dbAdapter = new DBAdapter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);
        Handler handler = new Handler();
        handler.postDelayed(new splashHandler(), 2000);

        // 設定データを保存するクラスのインスタンス化
        SharedPreferences pre = getSharedPreferences(CONFIG_DATA_SAVE, Context.MODE_PRIVATE);

        // 初回起動の場合、テキストからデータベースへ書き込みを行う
        if (pre.getBoolean(FIRST_DATA, true)) {

            // DBの読み込み
            dbAdapter.openDB();

            // テキストからデータベースへ書き込みを行う
            textToCategoryDB(dbAdapter);
            textToQuizDataDB(dbAdapter);

            // 2回目以降は処理を行わない様に初回起動用の設定データを"false"へ更新する
            SharedPreferences.Editor editor = pre.edit();
            editor.putBoolean(FIRST_DATA, false);
            editor.commit();

            // DBを閉じる
            dbAdapter.closeDB();
        }
    }

    class splashHandler implements Runnable {
        public void run() {
            Intent inte = new Intent(getApplication(), MainActivity.class);
            startActivity(inte);
            splash.this.finish();
        }
    }

    // テキストからカテゴリデータベースへの書き込みを行うメソッドの定義
    public void textToCategoryDB(DBAdapter dbAdapter) {

        try {
            // 入力ストリームの生成
            InputStream imputStream = this.getAssets().open(CATEGORY_TXT);

            // 入力ストリームのバッファリング
            BufferedReader tmpBuffere =
                    new BufferedReader(
                            new InputStreamReader(imputStream));

            // テキストの1行を格納する変数
            String line;

            // テキストファイルを最後まで読み込む
            while ((line = tmpBuffere.readLine()) != null) {

                // 各行を","で区切る
                String[] categoryTmp = line.split(COMMA, -1);

                // DBに登録
                dbAdapter.insertCategoryDB(categoryTmp);
            }

            // 入力ストリームのクローズ処理
            if (imputStream != null) {
                imputStream.close();
            }

            if (tmpBuffere != null) {
                tmpBuffere.close();
            }

        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        }
    }

    // テキストから問題集データベースへの書き込みを行うメソッドの定義
    public void textToQuizDataDB(DBAdapter dbAdapter) {

        try {
            // 入力ストリームの生成
            InputStream imputStream = this.getAssets().open(QUIZDATA_TXT);

            // 入力ストリームのバッファリング
            BufferedReader tmpBuffere =
                    new BufferedReader(
                            new InputStreamReader(imputStream));

            // テキストの1行を格納する変数
            String line;

            // テキストファイルを最後まで読み込む
            while ((line = tmpBuffere.readLine()) != null) {

                // 各行を","で区切る
                String[] quizDataTmp = line.split(COMMA, -1);

                // DBに登録
                dbAdapter.insertQuizDataDB(quizDataTmp);
            }

            // 入力ストリームのクローズ処理
            if (imputStream != null) {
                imputStream.close();
            }

            if (tmpBuffere != null) {
                tmpBuffere.close();
            }

        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        }
    }
}
