package com.example.sarak.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        // イメージボタンを設定
        ImageButton imageButtonCategory = (ImageButton) findViewById(R.id.categoryImgBtn);
        ImageButton imageButtonReport = (ImageButton) findViewById(R.id.reportImgBtn);
        ImageButton imageButtonConfig = (ImageButton) findViewById(R.id.configImgBtn);
        ImageButton imageButtonContact = (ImageButton) findViewById(R.id.contactImgBtn);

        imageButtonCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // カテゴリ画面推移の設定
                Intent intent = new Intent(getApplication(), CategoryActivity.class);
                startActivity(intent);
            }
        });

        imageButtonReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // 回答結果画面推移の設定
                Intent intent = new Intent(getApplication(), ReportActivity.class);
                startActivity(intent);
            }
        });

        imageButtonConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // 設定画面推移の設定
                Intent intent = new Intent(getApplication(), ConfigActivity.class);
                startActivity(intent);
            }
        });

        imageButtonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // お問い合わせ画面推移の設定
                Intent intent = new Intent(getApplication(), ContactActivity.class);
                startActivity(intent);
            }
        });
    }
}
